class Picture < ActiveRecord::Base
  include HasUUID

  belongs_to :user
  belongs_to :album

  #validates :name, presence: true
  validates :owner_id, presence: true
  #validates :likes, presence: true
  #validates :album_id, presence: true
  validates :encoded_pic, presence: true

end
