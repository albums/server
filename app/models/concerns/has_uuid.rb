module HasUUID
  extend ActiveSupport::Concern

  included do
    include ActiveUUID::UUID
  end

  module ClassMethods
    def primary_key
      'id'
    end
  end
end
