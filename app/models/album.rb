class Album < ActiveRecord::Base
  include HasUUID

  has_many  :pictures
  has_and_belongs_to_many  :users

  validates :name, presence: true
  validates :owner_id, presence: true
  validates :likes, presence: true
end
