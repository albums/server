class Friend < ActiveRecord::Base
  include HasUUID

  has_and_belongs_to_many :users

  validates :username, uniqueness: true, presence: true

end
