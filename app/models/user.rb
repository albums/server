class User < ActiveRecord::Base
  include HasUUID

  has_secure_password

  has_and_belongs_to_many :albums
  has_and_belongs_to_many :friends
  has_many  :pictures

  validates :email, uniqueness: true, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :username, uniqueness: true, presence: true

end
