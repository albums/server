json.array! @pictures do |picture|
	json.partial! picture, partial: 'pictures/picture'
end