class UsersController < ApplicationController
  before_action :set_user, except: [:index, :create, :login]

    # GET /users
    def index
      @users = User.all
    end

    # POST /users
    def create
      attrs = params.permit(:user_type, :first_name, :username, :last_name, :email, :password)

      @user = User.create(attrs)

      request = {
        id: @user.id,
        username: @user.username
      }

      friend = Friend.create(request)

      render_created @user
    end

    #PATCH /users/:id/friend
    def add_friend
      friend = Friend.find_by(username: params[:username])
      unless @user.friends.include? friend
        @user.friends.push(friend)

        curr_user = Friend.find(@user.id)
        temp = User.find_by(username: params[:username])

        temp.friends.push(curr_user)
        @user.save
      end
      render_updated @user
    end

    # PATCH /users/:id
    def update
    end

    # GET /users/:id
    def show
    end

    # POST /users/login
    def login
      @user = User.find_by_username(params[:username])
      @current_user = @user.authenticate(params[:password])
      if not @current_user
        render_errors('Login credentials invalid', :unauthorized)
      end
    end

    # GET /users/:id/albums
    def get_albums
      @albums = Album.where(owner_id: @user.id)
    end

    # GET /users/:id/contribute
    def contributing
      @albums = @user.albums
    end

    # GET /users/:id/newsfeed
    def newsfeed
      @pictures = []
      friends = @user.friends

      friends.each do |friend|
        user = User.find(friend.id)

        user_pics = Picture.where(owner_id: user.id)
        user_pics.each do |picture|
          @pictures.push(picture)
        end
      end
    end

    private
      def user_params
         params.require(:user).permit(:first_name, :last_name, :email, :username, :password)
      end

      def set_user
        @user = User.find(params[:id])

        if @user.nil?
         render_errors('Not a valid user id', :unauthorized)
        end
      end
end
