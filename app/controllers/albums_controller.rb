class AlbumsController < ApplicationController
  before_action :set_album, only: [:show, :update, :get_pictures]

    # GET /albums
    def index
      @albums = Album.all
    end

    # POST /users
    def create
      params = album_params
      owner = User.find(params[:owner_id])

      if owner.nil?
        render_unprocessable 'Invalid owner id'
      else
        @album = Album.create(params)
        render_created @album
      end
    end

    # PATCH /users
    def update
    end

    # GET /users/:id
    def show
    end

    # GET /albums/:id/pictures
    def get_pictures
      @pictures = Picture.where(album_id: @album.id)
    end

    private
      def album_params
         params.require(:album).permit(:name, :description, { user_ids: [] }, :owner_id)
      end

      def set_album
        @album = Album.find(params[:id])
      end

end
