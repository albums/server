class PicturesController < ApplicationController
  before_action :set_user, only: [:create]
  before_action :set_picture, only: [:show, :url]
  before_action :set_html, only: [:url]

    # GET /pictures
    def index
      @pictures = Picture.all
    end

    # POST /pictures
    def create
      url = 'http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/images/'
      params = picture_params

      owner = User.find(@owner_id)
      album = Album.find(params[:album_id])
      params[:name] = album.name
      params[:username] = owner.username
      @picture = Picture.create(params)

      unless @picture.nil?
        @picture.update_attributes(picture_url: url + @picture.id)
      end

      render_created @picture
    end

    # PATCH /pictures
    def update
    end

    # GET /pictures/:id
    def show
    end

    def url
      @raw = @picture.encoded_pic.tr('\\', '')
    end

    private
      def picture_params
         params.require(:picture).permit(:owner_id, :encoded_pic, :album_id)
      end

      def set_picture
        @picture = Picture.find(params[:id])
      end

      def set_user
        @owner_id = params[:owner_id]
      end

      def set_html
        request.format = 'html'
      end
end
