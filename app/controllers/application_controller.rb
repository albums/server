class ApplicationController < ActionController::Base
 before_action :set_format

  protected
    def set_format
      request.format = 'json'
    end

    def render_errors(errors, status)
      response.headers['Reflexion-Request-Errors'] = errors.to_s
      render json: { errors: errors }, status: status
    end

    def render_created(model)
      if model.errors.empty?
        render :show, status: :created
      else
        render_unprocessable model.errors.full_messages
      end
    end

    def render_updated(model, view: :show)
      if model.errors.empty?
        render view, status: :ok
      else
        render_errors(model.errors.full_messages, :unprocessable_entity)
      end
    end

    def render_unprocessable(error)
      render_errors [error], :unprocessable_entity
    end

    def render_unauthorized
      render_errors(['Access is denied due to invalid credentials'], :unauthorized)
    end
end
