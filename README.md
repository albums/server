# Bums Server

## Setup:
1. `bundle install'
2. `rails s` 

**Default PORT**: 3000. 

Prefix all HTTP requests with `localhost:8081`

## Routes:

**Login**: `POST` `/users`

*Example Request*: `request = {username: 'joeschmoe', password: 'joe123'}`

*Example Response*: `response = {user_id: '1'}`

**Individual User**: `GET` `/users/:id` 

*Example Request*: `/users/12`


## AWS:

### Server:

Public DNS: `ec2-54-148-129-99.us-west-2.compute.amazonaws.com`

Public IP: `54.148.129.99`

To access the server:

1. `$ ssh -i bumsAWSKey.pem ubuntu@54.148.129.99`

To make requests to server:

`GET/POST/ETC. ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/path`

Command-line

`curl ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/path`

... and more to be added.
