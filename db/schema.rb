# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150206012332) do

  create_table "albums", id: false, force: true do |t|
    t.uuid    "id",                      null: false
    t.string  "name",                    null: false
    t.string  "description",             null: false
    t.uuid    "owner_id",                null: false
    t.integer "likes",       default: 0, null: false
    t.binary  "user_ids"
    t.binary  "picture_ids"
  end

  add_index "albums", ["id"], name: "album_by_id", using: :btree
  add_index "albums", ["owner_id"], name: "albums_by_owner_id", using: :btree

  create_table "albums_users", id: false, force: true do |t|
    t.uuid "user_id"
    t.uuid "album_id"
  end

  create_table "friends", id: false, force: true do |t|
    t.uuid   "id",       null: false
    t.string "username", null: false
    t.binary "user_ids"
  end

  create_table "friends_users", id: false, force: true do |t|
    t.uuid "user_id"
    t.uuid "friend_id"
  end

  create_table "pictures", id: false, force: true do |t|
    t.uuid    "id",                      null: false
    t.string  "name"
    t.string  "description"
    t.string  "username"
    t.uuid    "owner_id"
    t.uuid    "album_id"
    t.integer "likes",       default: 0, null: false
    t.string  "picture_url"
    t.binary  "encoded_pic"
  end

  add_index "pictures", ["album_id"], name: "pictures_by_album_id", using: :btree
  add_index "pictures", ["id"], name: "picture_by_id", using: :btree
  add_index "pictures", ["owner_id"], name: "pictures_by_owner_id", using: :btree

  create_table "users", id: false, force: true do |t|
    t.uuid    "id",                          null: false
    t.string  "username",                    null: false
    t.string  "first_name",                  null: false
    t.string  "last_name",                   null: false
    t.string  "email",                       null: false
    t.string  "password_digest",             null: false
    t.integer "friends_count",   default: 0, null: false
    t.binary  "album_ids"
    t.binary  "friend_ids"
  end

  add_index "users", ["email"], name: "user_by_email", unique: true, using: :btree
  add_index "users", ["id"], name: "user_by_id", using: :btree
  add_index "users", ["username"], name: "user_by_username", unique: true, using: :btree

end
