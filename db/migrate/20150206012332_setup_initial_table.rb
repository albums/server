class SetupInitialTable < ActiveRecord::Migration
  def change
    create_users
    create_albums
    create_pictures
    create_friends
    create_friends_users_table
    create_albums_users_table
  end

  def create_users
    create_table :users, id: false do |t|
      t.uuid    :id,              null: false

      t.string  :username,         null: false
      t.string  :first_name,       null: false
      t.string  :last_name,        null: false
      t.string  :email,            null: false
      t.string  :password_digest,  null: false
      t.integer :friends_count,    null: false, default: 0
      t.binary  :album_ids
      t.binary  :friend_ids
    end

    add_index :users, :id, name: 'user_by_id'
    add_index :users, :email, unique: true, name: 'user_by_email'
    add_index :users, :username, unique: true, name: 'user_by_username'
  end

  def create_albums
    create_table :albums, id: false do |t|
      t.uuid    :id,                null: false

      t.string  :name,              null: false
      t.string  :description,       null: false
      t.uuid    :owner_id,          null: false
      t.integer :likes,             null: false, default: 0

      t.binary  :user_ids
      t.binary  :picture_ids
    end

    add_index :albums, :id, name: 'album_by_id'
    add_index :albums, :owner_id, name: 'albums_by_owner_id'
  end

  def create_pictures
    create_table :pictures, id: false do |t|
      t.uuid    :id,                null: false

      t.string  :album_name         null: false
      t.string  :description
      t.string  :username           null: false
      t.uuid    :owner_id
      t.uuid    :album_id
      t.integer :likes,             null: false,  default: 0
      t.string  :picture_url
      t.binary  :encoded_pic
    end

    add_index :pictures, :id, name: 'picture_by_id'
    add_index :pictures, :owner_id, name: 'pictures_by_owner_id'
    add_index :pictures, :album_id, name: 'pictures_by_album_id'
  end

  def create_friends
    create_table :friends, id: false do |t|
      t.uuid    :id,               null: false
      t.string  :username,         null: false
      t.binary  :user_ids
    end
  end

  def create_albums_users_table
    create_table :albums_users, id: false do |t|
      t.uuid  :user_id
      t.uuid  :album_id
    end
  end

  def create_friends_users_table
    create_table :friends_users, id: false do |t|
      t.uuid  :user_id
      t.uuid  :friend_id
    end
  end
end
