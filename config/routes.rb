Rails.application.routes.draw do
    scope module: 'public' do
    # Bums server root access page
    root to: 'root#info'
  end

  # Users Routes
  get '/users', to: 'users#index'
  get '/users/:id', to: 'users#show'
  get '/users/:id/albums', to: 'users#get_albums'
  get '/users/:id/contribute', to: 'users#contributing'
  get '/users/:id/newsfeed', to: 'users#newsfeed'
  post '/users', to: 'users#create'
  post '/users/login', to: 'users#login'
  patch '/users', to: 'users#update'
  patch '/users/:id/add_friend', to: 'users#add_friend'
  put '/users/:id/add_friend', to: 'users#add_friend'

  # Albums Routes
  get '/albums', to: 'albums#index'
  get '/albums/:id', to: 'albums#show'
  get '/albums/:id/pictures', to: 'albums#get_pictures'
  post '/albums', to: 'albums#create'
  patch '/albums', to: 'albums#update'

  # Pictures routes
  get '/pictures', to: 'pictures#index'
  get '/pictures/:id', to: 'pictures#show'
  get 'images/:id', to: 'pictures#url'
  post '/pictures', to: 'pictures#create'
  patch '/pictures', to: 'pictures#update'


end
